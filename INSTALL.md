# Como instalar e rodar essa aplicação

## Criando Virtualenv e instalando dependencias


### Criando virtualenv

É importante manter dependências de projetos separadas das usadas para as aplicações do sistema local e de outros projetos sendo desenvolvidos ou mantidos, e também para controle de quais versões de bibliotecas estão sendo utilizadas. Uma solução é usar o [virtualenv](https://virtualenv.pypa.io/en/stable/), que permite criar e gerenciar bibliotecas Python separadas com facilidade. Para ir além disso existe o projeto [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/index.html) que tem vários truques para facilitar o uso do virtualenv.

#### Usando o virtualenvwrapper:
	
	$ mkvirtualenv brbid_backend_challenge -r requirements.txt
	
Caso não entre no virtualenv criado diretamente

	$ workon brbid_backend_challenge
    
#### Apenas com o virtualenv:

	$ virtualenv ve 
    $ source ve/bin/activate
	$ pip install -r requirements.txt
  

## Para rodar a aplicação

	$ python manage.py migrate
	$ python manage.py runserver

Acesse [http://localhost:8000](http://localhost:8000) no navegador


Para parar o servidor de desenvolvimento e sair do virtualenv:

    CTRL + C

    $ deactivate


## Obtendo cobertura dos testes  

	$ pip install coverage
	$ coverage run --source='.' manage.py test
	$ coverage html
	$ cd htmlcov
	$ python -m http.server

Acesse [http://localhost:8000](http://localhost:8000) no navegador
