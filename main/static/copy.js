/* 
   Código para obter permissão e implmentar funcionalidade de copiar
   texto para o Clipboard

   Origem: em parte provavlemte do Stackoverflow mas não encontro o post exato
   no momento
 */

navigator.permissions.query({
    name: 'clipboard-write'
}).then(permissionStatus => {
    // Will be 'granted', 'denied' or 'prompt':
    console.log(permissionStatus.state);

    // Listen for changes to the permission state
    permissionStatus.onchange = () => {
        console.log(permissionStatus.state);
    };
});

function addCopyToClipboard(btn, textElement) {
    btn.onclick=function(){
        navigator.clipboard.writeText(textElement.textContent)
            .then(()=>{console.log("done")})
            .catch((err)=>{console.log(err)})
    }
}
