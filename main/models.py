import time
from urllib.parse import urlparse

from django.db import models
from django.conf import settings
from django.contrib.auth.models import User


#
# TODO: Talvez https://pypi.org/project/django-hashid-field/ fosse uma solução
# mais limpa para implmentar o id. Considerar no futuro
#


class ShortURL(models.Model):
    # Tamanho máximo aceitavel de acordo com: https://stackoverflow.com/questions/417142/what-is-the-maximum-length-of-a-url-in-different-browsers/417184#417184
    original = models.URLField(max_length=2000)
    shortened_id = models.BigIntegerField()
    user = models.ForeignKey(User, null=True, on_delete=models.SET_NULL)

    # FIXME: Talvez sobreescrever save fosse uma melhor solução
    @classmethod
    def create(cls, original):
        """
            Cria um objeto ShortURL e inicializa seu id com o timestamp atual
            Se isso fosse uma aplicação real talvez um valor aleatório (~salt~) pudesse ser somado ao timestamp para evitar chance de colisão
        """
        short_url = cls(original=original)
        short_url.shortened_id = int(time.time())
        return short_url

    def get_shortened_url(self):
        """
            Monta a URL curta codificando o id em hexadecimal
        """
        return '{}://{}/s/{}'.format(
            'http',
            settings.BASE_URL,
            hex(self.shortened_id)[2:]
        )

    @classmethod
    def find_by_short_id(cls, hex_id):
        """
            Para uma string, tenta converter em hexadecimal e obter um ShortURL com esse identificador
        """
        
        url_id = int('0x' + hex_id, 16)

        return cls.objects.get(shortened_id=url_id)

