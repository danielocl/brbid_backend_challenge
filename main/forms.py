from django.forms import modelform_factory, TextInput

from .models import ShortURL


ShortURLForm = modelform_factory(ShortURL, fields=("original",),
    labels={
        "original": ""
    },
    widgets={
    "original": TextInput()
})
