from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.contrib.auth.forms import UserCreationForm

from .forms import ShortURLForm
from .models import ShortURL

def home(request, short_id=None):

    short_url = ''

    if request.method == 'POST':
        form = ShortURLForm(request.POST)
        if form.is_valid():
            # FIXME: Para evitar refatorar o model ShortURL
            instance = form.instance.create(form.instance.original)
            if request.user.is_authenticated:
                instance.user = request.user
            instance.save()
            short_url = instance.get_shortened_url()
    else:
        if request.method == 'GET' and short_id != None:
            original_url = ShortURL.find_by_short_id(short_id).original
            return HttpResponseRedirect(original_url)
        else:
            form = ShortURLForm()

    context = {
        'message': 'Cole a sua URL abaixo',
        'form': form,
        'short_url': short_url
    }
    return render(request, 'main/index.html', context)


def register(request):
    if request.method == 'POST':
        f = UserCreationForm(request.POST)
        if f.is_valid():
            f.save()
            messages.success(request, 'Conta criada com sucesso')
            return redirect('home')

    else:
        f = UserCreationForm()

    return render(request, 'main/register.html', {'form': f})

