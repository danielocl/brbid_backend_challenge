import re, time
import unittest
from unittest import mock
from urllib.parse import urlparse

from django.test import TestCase
from django.conf import settings
from django.contrib.auth.models import User

from .models import ShortURL


# FIXME: usar a função reverse() ao invés do path da view diretamente


class HomeViewTests(TestCase):
    fixtures = ['test_data']
    
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)

    @unittest.skip("Exemplo de teste de view?")
    def test_view_return_hello_world_exclamation_point(self):
        expected_response = b"""<!DOCTYPE html>
<html lang="pt-BR">
    <body>
        Hello, World!
    </body>
</html>
"""

        response = self.client.get('/')

        self.assertEqual(response.status_code, 200)
        self.assertEquals(response.content, expected_response)

    def test_view_must_exist_and_return_200_on_GET(self):
        response = self.client.get('/')

        self.assertEqual(response.status_code, 200)

    @mock.patch('time.time', mock.MagicMock(return_value=12345))
    def test_view_must_return_a_short_url_when_given_a_url(self):
        url = "http://www.example.com"
        # como a URL curta é o timestamp o mock deve garantir que sempre seja a mesma
        expected_short_url = 'http://localhost:8000/s/3039'

        response = self.client.post('/', {
            'original': url
        })

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, expected_short_url)

    # TODO: test_invalid_url

    def test_redirect_to_original_url_given_a_short_url(self):
        """
            ShortURL usada nesse teste vem da fixture "test_data"
        """

        expected_redirect_target = "https://www.brbid.com/"
        short_url = "http://localhost:8000/s/5bba39ae"

        response = self.client.get(short_url, follow=True)
        self.assertTrue(response.redirect_chain)

        redirect_url, _ = response.redirect_chain[0]

        self.assertEquals(redirect_url, expected_redirect_target)

    # TODO:
    #   test_invalid_short_url
    #   test_nonexistent_short_url

    def test_shorturls_create_by_a_logged_user_should_be_related_to_it(self):
        url = "http://www.example.com"

        response = self.client.post('/login', self.credentials, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_active)

        response = self.client.post('/', {
            'original': url
        })

        user_short_urls = response.context['user'].shorturl_set.all()

        self.assertEqual(user_short_urls.count(), 1)

    def test_view_must_allow_for_urls_without_the_protocol(self):
        url = "www.example.com"

        response = self.client.post('/', {
            'original': url
        })

        self.assertEqual(response.status_code, 200)


class ShortURLTest(TestCase):

    def test_create_a_not_empty_short_url(self):
        url = 'http://www.example.com'

        short_url = ShortURL.create(url)
        shortened_url = short_url.get_shortened_url()

        self.assertNotEquals(shortened_url, "")

    def test_short_url_should_include_the_base_url(self):
        url = 'http://www.example.com'

        short_url = ShortURL.create(url)
        shortened_url = short_url.get_shortened_url()

        self.assertTrue(shortened_url.startswith('http://' + settings.BASE_URL))

    def test_short_url_id_should_not_be_empty(self):
        url = 'http://www.example.com'

        short_url = ShortURL.create(url)
        shortened_url = short_url.get_shortened_url()

        url_parts = urlparse(shortened_url)

        self.assertNotEqual(url_parts.path, '')

    def test_short_url_id_should_contain_a_valid(self):
        url = 'http://www.example.com'

        short_url = ShortURL.create(url)
        shortened_url = short_url.get_shortened_url()

        url_parts = urlparse(shortened_url)

        self.assertNotEqual(url_parts.path, '/None')

    def test_short_url_id_must_contain_letters(self):
        url = 'http://www.example.com'

        short_url = ShortURL.create(url)
        shortened_url = short_url.get_shortened_url()

        url_parts = urlparse(shortened_url)

        # É preciso ignorar '/s/' no início do path
        self.assertNotEqual(re.search('\D', url_parts.path[3:]), None, 'Não contem letras')

    def test_short_url_must_be_only_alphanumeric(self):
        url = 'http://www.example.com'

        short_url = ShortURL.create(url)
        shortened_url = short_url.get_shortened_url()

        url_parts = urlparse(shortened_url)

        # É preciso ignorar '/s/' no início do path
        #import pdb; pdb.set_trace()
        self.assertEqual(
            re.search('[^0-9a-zA-Z]', url_parts.path[3:]), None,
            url_parts.path + ' contém uma caracter que não é letra nem número'
        )

    def test_recover_original_url_from_short_one(self):
        url = 'http://www.example.com'

        short_url = ShortURL.create(url)
        short_url.save()

        shortened_url = short_url.get_shortened_url()

        url_parts = urlparse(shortened_url)

        # É preciso ignorar '/s/' no início do path
        base64_encoded_id = url_parts.path[3:]

        recovered_short_url = ShortURL.find_by_short_id(base64_encoded_id)

        self.assertEquals(url, recovered_short_url.original)
 
    @unittest.skip('Qual o comportamento esperado aqui?')
    def test_recover_a_non_existent_url(self):
        shortened_url = 'http://www.example.com/AAAAAFu5Ghg='

        url_parts = urlparse(shortened_url)

        # É preciso ignorar '/s/' no início do path
        base64_encoded_id = url_parts.path[3:]

        recovered_short_url = ShortURL.find_by_short_id(base64_encoded_id)


class LoginViewTests(TestCase):

    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)

    def test_login(self):
        response = self.client.post('/login', self.credentials, follow=True)

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context['user'].is_active)

